#!/bin/bash

# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Bold
BBlack='\033[1;30m'       # Black
BRed='\033[1;31m'         # Red
BGreen='\033[1;32m'       # Green
BYellow='\033[1;33m'      # Yellow
BBlue='\033[1;34m'        # Blue
BPurple='\033[1;35m'      # Purple
BICyan='\033[1;36m'        # Cyan
BWhite='\033[1;37m'       # White

# Underline
UBlack='\033[4;30m'       # Black
URed='\033[4;31m'         # Red
UGreen='\033[4;32m'       # Green
UYellow='\033[4;33m'      # Yellow
UBlue='\033[4;34m'        # Blue
UPurple='\033[4;35m'      # Purple
UCyan='\033[4;36m'        # Cyan
UWhite='\033[4;37m'       # White

# Background
On_Black='\033[40m'       # Black
On_Red='\033[41m'         # Red
On_Green='\033[42m'       # Green
On_Yellow='\033[43m'      # Yellow
On_Blue='\033[44m'        # Blue
On_Purple='\033[45m'      # Purple
On_Cyan='\033[46m'        # Cyan
On_White='\033[47m'       # White

# High Intensity
IBlack='\033[0;90m'       # Black
IRed='\033[0;91m'         # Red
IGreen='\033[0;92m'       # Green
IYellow='\033[0;93m'      # Yellow
IBlue='\033[0;94m'        # Blue
IPurple='\033[0;95m'      # Purple
ICyan='\033[0;96m'        # Cyan
IWhite='\033[0;97m'       # White

# Bold High Intensity
BIBlack='\033[1;90m'      # Black
BIRed='\033[1;91m'        # Red
BIGreen='\033[1;92m'      # Green
BIYellow='\033[1;93m'     # Yellow
BIBlue='\033[1;94m'       # Blue
BIPurple='\033[1;95m'     # Purple
BICyan='\033[1;96m'       # Cyan
BIWhite='\033[1;97m'      # White

# High Intensity backgrounds
On_IBlack='\033[0;100m'   # Black
On_IRed='\033[0;101m'     # Red
On_IGreen='\033[0;102m'   # Green
On_IYellow='\033[0;103m'  # Yellow
On_IBlue='\033[0;104m'    # Blue
On_IPurple='\033[0;105m'  # Purple
On_ICyan='\033[0;106m'    # Cyan
On_IWhite='\033[0;107m'   # White

#curl "https://gitlab.com/hellrja/tf/-/raw/main/tf.sh?ref_type=heads&inline=false" -o tf; chmod u+x tf; mv -f tf $HOME/bin;

export SLEEP_TIME=5
unset tf_SUGGESTIONS_FOUND

# Terraform doesn't use gcloud auth login and instead uses application default credentials which is very easy to forget and remediate
export GCLOUD_ADC_FILE="$HOME/.config/gcloud/application_default_credentials.json"
if [ ! -e $GCLOUD_ADC_FILE ]; then
    echo -e "${BICyan}────────────────────────────────────────────────────────────────────────────────────${Color_Off}"
    echo -e "${Color_Off}gcloud's Application Default Credential (ADC) does not appear set and is required for terraform to succeed.${Color_Off}"
    echo -e
    echo -e "${Color_Off}Simply logging into ${On_White}gcloud auth login${Color_Off}${Color_Off} isn't good enough - you must also login as follows:${Color_Off}"
    echo -e
    echo -e "${Red}${On_White}gcloud auth application-default login${Color_Off}"
    export tf_SUGGESTIONS_FOUND="true"
else
    if [[ $((($(date +%s) - $(date +%s -r "/home/hellrja/.config/gcloud/application_default_credentials.json")) / 86400)) -gt 14 ]]; then
        echo -e "${BICyan}────────────────────────────────────────────────────────────────────────────────────${Color_Off}"
        echo -e "${Color_Off}Your gcloud Application Default Credentials appear stale; you may need to run this again : ${Red}${On_White}gcloud auth application-default login${Color_Off}"
        export tf_SUGGESTIONS_FOUND="true"
    fi
fi

# If you don't have a .gitignore and you (use this script to) run a terraform plan it leaves residue behind that would then get pushed to the remote repo (on gitlab) and that will have unintended and negative consequences to gitlab pipelines
if [ -e "../.git" ] && [ ! -e "../.gitignore" ]; then
    echo -e "${BICyan}────────────────────────────────────────────────────────────────────────────────────${Color_Off}"
    echo -e "${Color_Off}You may want to consider adding a ${Red}${On_White}.gitignore${Color_Off}${Color_Off} file to the root of this repo. This will prevent the residue from terraform plan from being pushed to the remote repo.${Color_Off}"
    echo -e
    echo -e "${Color_Off}The content of ${Red}${On_White}.gitignore${Color_Off}${Color_Off} should, at a minimum, contain:${Color_Off}"
    echo -e "${Red}${On_White}**/.terraform/*${Color_Off}"
    echo -e "${Red}${On_White}.terraform.lock.hcl${Color_Off}"
    echo -e "${Red}${On_White}*.no${Color_Off}"
    echo -e "${Red}${On_White}**/*tfstate*${Color_Off}"
    export tf_SUGGESTIONS_FOUND="true"
fi

# Our pipelines typically require a vars.tfvars file in a subfolder and if the user didn't provide one they probably didn't mean to so remind them
export VAR_FILE="false"
for var in "$@"; do
    if [[ "$var" = "-var-file"* ]]; then
        export VAR_FILE="true"
    fi
done
if [ $VAR_FILE == "false" ]; then
    echo -e "${BICyan}────────────────────────────────────────────────────────────────────────────────────${Color_Off}"
    echo -e "${Color_Off}You did not pass a -var-file parameter; you may want to add : -var-file=<path to vars file>${Color_Off}"
    echo -e
    echo -e "For example ${Red}${On_White}-var-file=./environments/dev/vars.tfvars${Color_Off}"
    export tf_SUGGESTIONS_FOUND="true"
fi

# The current directory must be the location where the terraform code is; it's easy to mistakenly run this without so remind the user
if ! ls *.tf 1> /dev/null 2>&1; then
    echo -e "${BICyan}────────────────────────────────────────────────────────────────────────────────────${Color_Off}"
    echo -e "${Color_Off}No .tf file found in current folder, you probably want to ${Red}${On_White}cd${Color_Off}${Color_Off} to a folder with the terraform code you want to test.${Color_Off}"
    export tf_SUGGESTIONS_FOUND="true"
fi

# Check if a VAULT_TOKEN was set; don't trigger a pause though as this is less common
if [ -z "$VAULT_TOKEN" ]; then
    echo -e "${BICyan}────────────────────────────────────────────────────────────────────────────────────${Color_Off}"
    echo -e "${Color_Off}VAULT_TOKEN has not been set; if your code retrieves secrets from Vault you should login to Vault (note that jq is required for the below command) :${Color_Off}"
    echo -e
    echo -e "${Red}${On_White}export VAULT_TOKEN=\$(export VAULT_ADDR=https://vault.keybank.com; vault login -method=ldap mount=ad username=<USERID> -format=json | jq -r '.auth.client_token')${Color_Off}${Color_Off}"
    if [ ! "${tf_SUGGESTIONS_FOUND}" == "true" ]; then
        echo -e "${BICyan}────────────────────────────────────────────────────────────────────────────────────${Color_Off}"
    fi
fi

# Check if tf has any suggestions for the user and pause if so, so they can see them better
if [ "${tf_SUGGESTIONS_FOUND}" == "true" ]; then
    echo -e "${BICyan}────────────────────────────────────────────────────────────────────────────────────${Color_Off}"
    echo -e "${Color_Off}Pausing for ${SLEEP_TIME} seconds for you to read tf's suggestions.${Color_Off}"
    echo -e "${BICyan}────────────────────────────────────────────────────────────────────────────────────${Color_Off}"
    sleep ${SLEEP_TIME}s
fi

# The secret sauce of this script is to simply rename the backend.tf file which forces terraform to default to using a local state
if [ -e "backend.tf" ]; then
    mv backend.tf backend.no
fi

# Drum roll please!
terraform $1 $2 $3 $4 $5 $6 $7 $8 $9

# Undo the secret sauce, putting humpty back together again (hopefully .gitignoring the .terraform and .terraform.lock.hcl folders/files).
if [ -e "backend.no" ]; then
    mv backend.no backend.tf
fi
