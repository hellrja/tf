
#Copy / paste command to download & install
#Invoke-WebRequest "https://gitlab.com/hellrja/tf/-/raw/main/tf.ps1?ref_type=heads&inline=false" -OutFile $ENV:USERPROFILE\AppData\Local\Microsoft\WindowsApps\tf.ps1 -UserAgent "Mozilla/5.0 (Windows NT 10.0; Microsoft Windows 10.0.15063; en-US) PowerShell/6.0.0" -Proxy http://vzproxy.keybank.com:80

$SLEEP_TIME = 5
$tf_SUGGESTIONS_FOUND = $false

# Terraform doesn't use gcloud auth login and instead uses application default credentials which is very easy to forget and remediate
if ((Test-Path "$ENV:USERPROFILE\AppData\Roaming\gcloud\application_default_credentials.json") -Eq $False) {
    write-host -ForegroundColor Cyan "────────────────────────────────────────────────────────────────────────────────────"
    Write-Host "gcloud's Application Default Credential (ADC) does not appear set and is required for terraform to succeed."
    Write-Host
    Write-Host "Simply logging into gcloud auth login isn't good enough - you must also login as follows:"
    Write-Host
    Write-Host "gcloud auth application-default login"
    $tf_SUGGESTIONS_FOUND = $true
}
else {
    $Now = (Get-Date)
    $AdcLastWriteTime = Get-Date((Get-ChildItem "$ENV:USERPROFILE\AppData\Roaming\gcloud\application_default_credentials.json" | Select-Object LastWriteTime).LastWriteTime)
    $DateDelta = $Now - $AdcLastWriteTime

    If ($DateDelta.TotalDays -gt 14) {
        Write-Host -ForegroundColor Cyan "────────────────────────────────────────────────────────────────────────────────────"
        Write-Host "Your gcloud Application Default Credentials appear stale; you may need to run this again : gcloud auth application-default login"
        $tf_SUGGESTIONS_FOUND = $true
    }       
}

# If you don't have a .gitignore and you (use this script to) run a terraform plan it leaves residue behind that would then get pushed to the remote repo (on gitlab) and that will have unintended and negative consequences to gitlab pipelines
If ((Test-Path "..\.git") -And (Test-Path "..\.gitignore") -Eq $False) {
    Write-Host -ForegroundColor Cyan "────────────────────────────────────────────────────────────────────────────────────"
    Write-Host "You may want to consider adding a .gitignore file to the root of this repo. This will prevent the residue from terraform plan from being pushed to the remote repo."
    Write-Host
    Write-Host "The content of .gitignore should, at a minimum, contain:"
    Write-Host "**/.terraform/*"
    Write-Host ".terraform.lock.hcl"
    Write-Host "*.no"
    Write-Host "**/*tfstate*"
    $tf_SUGGESTIONS_FOUND = $true
}

# Our pipelines typically require a vars.tfvars file in a subfolder and if the user didn't provide one they probably didn't mean to so remind them
$VAR_FILE = $False
ForEach ($Arg In $Args) {
    $ArgToString = $Arg.ToString().ToUpper().Trim()
    If ($ArgToString.Contains("-VAR-FILE")) {
        $VAR_FILE = $true
        # break 
    }
}
If ($VAR_FILE -eq $false) {
    Write-Host -ForegroundColor Cyan "────────────────────────────────────────────────────────────────────────────────────"
    Write-Host "You did not pass a -var-file parameter; you may want to add : -var-file=<path to vars file>"
    Write-Host
    Write-Host "For example -var-file=./environments/dev/vars.tfvars"
    $tf_SUGGESTIONS_FOUND = $true
}

# The current directory must be the location where the terraform code is; it's easy to mistakenly run this without so remind the user
If ((Test-Path ".\*.tf") -Eq $false) {
    Write-Host -ForegroundColor Cyan "────────────────────────────────────────────────────────────────────────────────────"
    Write-Host "No .tf file found in current folder, you probably want to cd to a folder with the terraform code you want to test."
    $tf_SUGGESTIONS_FOUND = $true
}

# Check if a VAULT_TOKEN was set; don't trigger a pause though as this is less common
If ($env:VAULT_TOKEN -Eq $Null) {
    Write-Host -ForegroundColor Cyan "────────────────────────────────────────────────────────────────────────────────────"
    Write-host "VAULT_TOKEN has not been set; if your code retrieves secrets from Vault you should login to Vault :"
    Write-Host
    Write-Host '$env:VAULT_ADDR = "https://vault.keybank.com"; $env:VAULT_TOKEN = (vault login -method=ldap mount=ad username=hellrja -format=json | ConvertFrom-Json).auth.client_token'
    If ($tf_SUGGESTIONS_FOUND -eq $False) {
        Write-Host -ForegroundColor Cyan "────────────────────────────────────────────────────────────────────────────────────"
    }
}

# Check if tf has any suggestions for the user and pause if so, so they can see them better
If ($tf_SUGGESTIONS_FOUND) {
    Write-Host -ForegroundColor Cyan "────────────────────────────────────────────────────────────────────────────────────"
    Write-Host "Pausing for $SLEEP_TIME seconds for you to read tf's suggestions."
    Write-Host -ForegroundColor Cyan "────────────────────────────────────────────────────────────────────────────────────"
    Start-Sleep -Seconds $SLEEP_TIME
}

# The secret sauce of this script is to simply rename the backend.tf file which forces terraform to default to using a local state
If (Test-Path ".\backend.tf") {
    mv backend.tf backend.no
}

# Drum roll please!
terraform $args

# Undo the secret sauce, putting humpty back together again (hopefully .gitignoring the .terraform and .terraform.lock.hcl folders/files).
If (Test-Path ".\backend.no") {
    mv backend.no backend.tf
}
